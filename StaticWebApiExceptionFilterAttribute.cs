﻿using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Web.Http.Filters;

namespace StudioKit.ErrorHandling.WebApi
{
	/// <summary>
	/// An exception filter to handle all System.Web.Http namespace exceptions (e.g. Web API).
	/// Requires the static <see cref="ErrorHandler.Instance"/> to be set in order to work.
	/// Alternative approach versus using Dependency Injection as in <see cref="WebApiExceptionFilterAttribute"/>
	/// </summary>
	public class StaticWebApiExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilterAttribute
	{
		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			base.OnException(actionExecutedContext);

			var exception = actionExecutedContext.Exception;
			if (IsExceptionIgnored(exception))
				return;

			if (exception != null)
				ErrorHandler.Instance?.CaptureException(exception, actionExecutedContext.GetExceptionUser());
		}

		/// <summary>
		/// Override point for defining which exceptions or types of exceptions are ignored.
		/// </summary>
		/// <param name="exception">The exception</param>
		/// <returns></returns>
		public virtual bool IsExceptionIgnored(Exception exception)
		{
			return exception is OperationCanceledException;
		}
	}
}