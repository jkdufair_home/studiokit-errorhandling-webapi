﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.ErrorHandling.WebApi
{
	/// <summary>
	/// DelegatingHandler which processes HttpResponses.
	/// Looks for failing HttpStatusCode >= 400 which returns an error object.
	/// Converts ODataErrors, HttpErrors to custom ApiErrorMessage object
	/// </summary>
	public class ErrorMessageHandler : DelegatingHandler
	{
		private readonly HttpConfiguration _httpConfiguration;
		private readonly bool _displayMessageDetails;

		public ErrorMessageHandler(HttpConfiguration httpConfiguration, bool displayMessageDetails = false)
		{
			_httpConfiguration = httpConfiguration;
			_displayMessageDetails = displayMessageDetails;
		}

		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
			CancellationToken cancellationToken)
		{
			return base.SendAsync(request, cancellationToken).ContinueWith(
				task =>
				{
					if (!ResponseIsErrorObject(task.Result))
						return task.Result;
					task.Result.TryGetContentValue(out object responseObject);
					TryConvertObjectToApiErrorMessage(request, responseObject, task.Result);
					return task.Result;
				}, cancellationToken);
		}

		private static bool ResponseIsErrorObject(HttpResponseMessage response)
		{
			return response != null && (int)response.StatusCode >= 400 && response.Content is ObjectContent;
		}

		private void TryConvertObjectToApiErrorMessage<T>(HttpRequestMessage request, T responseObject, HttpResponseMessage response) where T : class
		{
			const string shardExceptionType =
				"Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement.ShardManagementException";
			var errorMessage = responseObject as ApiErrorMessage;
			var formatter = _httpConfiguration.Formatters.First(t =>
				t.SupportedMediaTypes.Contains(new MediaTypeHeaderValue(response.Content.Headers.ContentType.MediaType)));

			// convert HttpError to ApiErrorMessage
			var httpError = responseObject as HttpError;
			if (httpError != null)
			{
				if (httpError.ExceptionType == shardExceptionType)
				{
					response.StatusCode = HttpStatusCode.Gone;
				}
				errorMessage = GetApiErrorMessage(response.StatusCode, httpError);
			}

			if (errorMessage == null) return;

			response.Content = new ObjectContent<ApiErrorMessage>(errorMessage, formatter);
		}

		private ApiErrorMessage GetApiErrorMessage(HttpStatusCode statusCode, HttpError httpError)
		{
			return new ApiErrorMessage(statusCode, "Error", GetHttpErrorMessage(httpError));
		}

		private string GetHttpErrorMessage(HttpError httpError)
		{
			return $"{httpError.Message}" +
					$"{(_displayMessageDetails && !string.IsNullOrEmpty(httpError.ExceptionMessage) ? "\n\n" + httpError.ExceptionMessage : "")}" +
					$"{(_displayMessageDetails && !string.IsNullOrEmpty(httpError.StackTrace) ? "\n\n" + httpError.StackTrace : "")}" +
					$"{(_displayMessageDetails && httpError.InnerException != null ? "\n\n" + GetHttpErrorMessage(httpError.InnerException) : "")}";
		}
	}
}