﻿using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Web.Http.Filters;

namespace StudioKit.ErrorHandling.WebApi
{
	/// <summary>
	/// This is a dummy filter to be replaced by HttpExceptionFilterFilter via Ninject.
	/// We need this so that we can properly inject into HttpExceptionFilterFilter.
	/// </summary>
	public class WebApiExceptionFilterAttribute : FilterAttribute
	{
	}

	/// <summary>
	/// An exception filter to handle all System.Web.Http namespace exceptions (e.g. Web API).
	///
	/// Ninject should bind <see cref="WebApiExceptionFilterAttribute"/>
	/// to <see cref="WebApiExceptionFilter"/>.
	///
	/// <![CDATA[kernel.BindFilter<WebApiExceptionFilter>(FilterScope.Controller, 0).WhenControllerHas<WebApiExceptionFilterAttribute>();]]>
	///
	/// An <see cref="IErrorHandler"/> must be injected.
	/// </summary>
	public class WebApiExceptionFilter : ExceptionFilterAttribute, IExceptionFilterAttribute
	{
		private readonly IErrorHandler _errorHandler;

		public WebApiExceptionFilter(IErrorHandler errorHandler)
		{
			_errorHandler = errorHandler;
		}

		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			base.OnException(actionExecutedContext);

			var exception = actionExecutedContext.Exception;
			if (IsExceptionIgnored(exception))
				return;

			if (exception != null)
				_errorHandler.CaptureException(exception, actionExecutedContext.GetExceptionUser());
		}

		/// <summary>
		/// Override point for defining which exceptions or types of exceptions are ignored.
		/// </summary>
		/// <param name="exception">The exception</param>
		/// <returns></returns>
		public virtual bool IsExceptionIgnored(Exception exception)
		{
			return exception is OperationCanceledException;
		}
	}
}