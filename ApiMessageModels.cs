﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Runtime.Serialization;
using System.Web.Http.ModelBinding;

namespace StudioKit.ErrorHandling.WebApi
{
	/// <summary>
	/// Class for returning API messages
	/// </summary>
	[DataContract]
	public class ApiMessage
	{
		/// <summary>
		/// API Message Constructor
		/// </summary>
		/// <param name="statusCode">The HTTP status code to embed in the object</param>
		/// <param name="title">The title to display to the API user</param>
		/// <param name="message">The message to display to the API user</param>
		public ApiMessage(HttpStatusCode statusCode, string title, string message)
		{
			Code = statusCode;
			Title = title;
			Message = message;
		}

		[DataMember(Name = "title")]
		public string Title { get; set; }

		[DataMember(Name = "message")]
		public string Message { get; set; }

		[DataMember(Name = "code")]
		public HttpStatusCode Code { get; set; }
	}

	/// <summary>
	/// Class for returning API errors
	/// </summary>
	[DataContract]
	public class ApiErrorMessage : ApiMessage
	{
		/// <summary>
		/// Error Message Constructor
		/// </summary>
		/// <param name="statusCode">The HTTP status code to embed in the object</param>
		/// <param name="title">The title to display to the API user</param>
		/// <param name="message">The message to display to the API user</param>
		public ApiErrorMessage(HttpStatusCode statusCode, string title, string message)
			: base(statusCode, title, message)
		{
			if ((int)statusCode < 400)
				throw new ArgumentOutOfRangeException(nameof(statusCode), "statusCode must be greater than or equal to 400");
		}
	}

	public static class HttpRequestMessageExtensions
	{
		public static HttpResponseMessage CreateResponse(this HttpRequestMessage request, ApiMessage apiMessage)
		{
			if (apiMessage == null)
				throw new ArgumentNullException(nameof(apiMessage));

			return new HttpResponseMessage
			{
				StatusCode = apiMessage.Code,
				Content = new ObjectContent<ApiMessage>(apiMessage, new JsonMediaTypeFormatter())
			};
		}

		public static HttpResponseMessage CreateResponse(this HttpRequestMessage request, ApiErrorMessage apiErrorMessage)
		{
			if (apiErrorMessage == null)
				throw new ArgumentNullException(nameof(apiErrorMessage));

			return new HttpResponseMessage
			{
				StatusCode = apiErrorMessage.Code,
				Content = new ObjectContent<ApiErrorMessage>(apiErrorMessage, new JsonMediaTypeFormatter())
			};
		}

		public static HttpResponseMessage CreateResponse(this HttpRequestMessage request, ModelStateDictionary modelState)
		{
			if (modelState == null)
				throw new ArgumentNullException(nameof(modelState));

			var errors = modelState.Values.SelectMany(x => x.Errors);
			var messages =
				errors.Select(
					x => (x.ErrorMessage ?? "") + (x.Exception?.Message ?? ""));
			var messageString = string.Join(";", messages);
			var apiErrorMessage = new ApiErrorMessage(HttpStatusCode.BadRequest, "Error",
				$"Could not accept request. Reason(s): {messageString}");

			return new HttpResponseMessage
			{
				StatusCode = HttpStatusCode.BadRequest,
				Content = new ObjectContent<ApiErrorMessage>(apiErrorMessage, new JsonMediaTypeFormatter())
			};
		}
	}
}