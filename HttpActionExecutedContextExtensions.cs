﻿using StudioKit.ErrorHandling.Interfaces;
using System.Web.Http.Filters;

namespace StudioKit.ErrorHandling.WebApi
{
	public static class HttpActionExecutedContextExtensions
	{
		public static IExceptionUser GetExceptionUser(this HttpActionExecutedContext actionExecutedContext)
		{
			return actionExecutedContext.ActionContext.RequestContext.Principal?.Identity.ToExceptionUser();
		}
	}
}